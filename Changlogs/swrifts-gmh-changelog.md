# Rifts® for Savage Worlds Gamemaster's Handbook

## 2.0.0 (04 October 2022)

- Update module for Foundry v10 Compatibility.
- Requires SWADE System, version 2.0.3+
- Requires FoundryVTT v10.286+

## Changes

- Update Journal Entries and Entity Links to Foundry v10 Compatibility
- Provide stable permanent UUIDs for Journal Entries
- Add Compendium Table of Contents formatting
- Fix errors in some rolltables

## Initial Release 1.0.1

### Fixes

- Add Macros for Adventure and Settlement Generators
- Break apart "The Plot Thickens" and "Other Authority Figure" 

