# Rifts® for Savage Worlds Savage Foes of North America

# 2.0.0 (October 4, 2022)

- Update module for Foundry v10 Compatibility.
- Requires SWADE System, version 2.0.3+
- Requires FoundryVTT v10.286+

## Notes
- The new actor Compendium Table of Contents takes a moment to load as it displays tokens for actors in the display.

## Changes
- Update Journal Entries and Entity Links to Foundry v10 Compatibility
- Provide stable permanent UUIDs for Journal Entries
- Add Compendium Table of Contents formatting
- Add alternative firing modes (eg Heavy Beam) and blast templates (eg Grenades) to eligible weapons
- Update all actors to relfect equipment changes from *Savage Foes* and *Tomorrow Legion Players Guide**
- Fix data entry errors on some actors

# Initial Release 1.0.1

## Fixes

- Fix Vehicle wounds based on size
- Fix Vehicles weapons to be on the vehicle
- Remove empty db files from the package
- Fix Xiticix Flight pace
- Add broken out abilities from Iconic Frameworks to Savage Foes
