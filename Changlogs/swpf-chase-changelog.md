# Savage Pathfinder Chase Deck

## Initial Release 1.0.0

- 54 Cards in a deck.
- Custom Hand sheet to make it easier to play cards and pass them to other players.
- Macro to set up hands. Make sure to first set up your player accounts.
- Instructions on how to use this module, as well as do what the macro does by hand.
