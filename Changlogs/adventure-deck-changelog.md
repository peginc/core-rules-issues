# Savage Worlds Adventure Deck

## Version 2.0.0 (October 4, 2022)

- Updated module for FoundryVTT v10
- Added option to "announce cards" that are drawn.  If selected the cards that are dealt are announced in chat as they are dealt.
- Removed Shuffle and Reset from every deal and added buttons to shuffle and reset.
- Fixed error where player could not play a card to the discard pile due to lack of owner permissions on the pile.

## Version 1.0.1 (March 15, 2022)

- Added the ability to display the artwork from a card by clicking on the card face in the hand sheet.
- Changed all card names to `Title Case`
- Setup script adds `Observer` permission to all player hands to facilitate trading cards
- Setup script no longer considers Players with Role of `none`
- Update Instructions for clarity and add support e-mail address

## Initial Release 1.0.0

- 53 Cards in a deck.
- Custom Hand sheet to make it easier to play cards and pass them to other players.
- Macro to set up hands. Make sure to first set up your player accounts.
- Instructions on how to use this module (54th card), as well as do what the macro does by hand.
- *Destiny's Child* Edge.
