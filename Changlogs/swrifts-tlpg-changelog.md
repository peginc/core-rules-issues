# Rifts® for Savage Worlds Tommorow Legion Player's Guide

## 2.0.0 (04 October 2022)

- Update module for Foundry v10 Compatibility.
- Requires SWADE System, version 2.0.3+
- Requires FoundryVTT v10.286+

## Changes

- Update Journal Entries and Entity Links to Foundry v10 Compatibility
- Provide stable permanent UUIDs for Journal Entries
- Fixed incorrect header image on Character Sheet Gear Tab
- Add Compendium Table of Contents formatting
- Remove Compendium Folders dependency
- Add Psi-Sword, Psi-Blade, and Psi-Shield Items
- Add *Rifts® for Savage Worlds* Archetypes with updated styling for stat blocks
- Add image journal with *Welcome to the Post Apocalypse* to introduce new players to the setting
- Add alternative firing modes (eg Heavy Beam) and blast templates (eg Grenades) to eligible weapons
- Add Active Effects to eligible Edges, Cybernetics, and Gear

## 1.0.2 (07 September 2022)

### Fixes

- Add missing Iconic Edges
- Fix Iceblast shotgun to have +2 shooting
- Fix coloring of text for skill descriptions as well as skill die icons on the Rifts character sheet
- Add Invisibility ability to Brodkil
- C-18 laser stats fixed
- Fixed closecombat roll table to include description
- Fixed Ion weapons to have +2 to shooting roll
- Laser weapons no longer have recoil for full auto
- Added skill icons for Battle and Research
- Cyberknight fixes for not using fleet footed and instead adjusting pace
- Created missing fortune and Glory Table

