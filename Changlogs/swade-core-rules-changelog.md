# 1.0.7

## New Features

- Completely refactored **Patch All Actors** macro.  Allowing you to choose which actors, folders and compendiums to patch against!
- Included the **Savage Worlds Action Deck**!
- Included all 6 of the **Savage Worlds Bennies and bumpmap images** to go with them, so you can update your 3d bennies to any of them you like! 

## Fixes

- Add **V9 Support** to module
- Fix **Brawny** and **Soldier** edges to adjust encumbrance correctly with active effects
- Fix **Fear Table Macro** for V9 support
- Fix **Luck**, **Great Luck**, and **Bad Luck** edges and hindrances to adjust bennies correctly
- Fix **Elderly** Hindrance active effects
- Fix **Bullet weights** to be correct based on quantity

