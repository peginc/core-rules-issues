# 1.0.5

## Enchancements

- Update Module to be 0.8 compatable
- Patch All actors now updates the skill icon as part of the patching
- Add Ammo type to the weapons for ammo management

## Fixes

- Beastiary entries removed star in core skills
- Patch All Actors script fixed to actually update descriptions correctly
- Fix broken link in Persuasion skill
- Fix load errors in 0.8 by updating javascript
- Fix Maul Damage.
- Fix Interludes Table having Hearts twice instead of clubs.
- Fix Sling stones weight
- Fix Improved edges to match the names in the Compendium
- Fix Guardians to no longer have Fly listed
- Remove Saurians low light vision
- Fix Small Shield Cover
- Add Athletics skill to Breath Weapons
- Resolve typo in Entangle power
- Fix typo in Quick Encounters
- Fix typos in Puppet Power Description
- Rename Crossbow items to match name in rules
- Fix links to Power Modifiers
- Fix Ammo weights
- Fix typos in McGyver
- Fix various issues in Grenades
- Remove actor link for bestiary actors
