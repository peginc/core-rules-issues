# Welcome to *Savage Worlds Adventure Edition*

# Table of Contents
1. [Introduction](#Introduction)
2. [Purchasing the Module](#Purchasing-The-Module)
3. [Installation](#Installation)
4. [Content Overview](#Content-Overview)  
    1. [Rules Compendium](#Rules-Compendium) 
    2. [Actor Compendiums](#Actor-Compendiums)
    3. [Item Compendiums](#Item-Compendiums)
    4. [Roll Tables](#Roll-Tables)
5. [Macros](#Macros)
6. [Module Settings](#Module-Settings)
6. [Other Modules](#Other-Modules)
7. [FAQ](#FAQ)
8. [Support](#Support)
9. [License](#License)

# Introduction
Welcome to the first official *Savage Worlds* premium content pack for Foundry Virtual Tabletop. Within, please find the complete, fully interlinked *Savage Worlds* Adventure Edition core rules. Additionally, you will find all art from the core rule book as well as more than 200 additional art pieces. Many other goodies from the core rules are included as well, such as  automated roll tables, ready-to-go Items, weapons with pre configured actions,  and tokenized Actors and Vehicles.

If you are already playing *Savage Worlds* on Foundry VTT with the free *Savage Worlds* Adventure Edition [system package](https://foundryvtt.com/packages/swade/), this module includes a macro that will update all Actors with the new full-text-and art versions of their existing Items.

We hope you enjoy this labor of love and that your games are *Fast, Furious, and Fun!*

# Purchasing The Module
1. <b>Buying a license key for the module</b>  
You can purchase a license key and supporting documentation for the *Savage Worlds* Adventure Edition Core Rules module directly from the PEG license store as an electronic download.

2. <b>Activate the module within Foundry</b>  
To activate your copy of the module, log into foundryvtt.com and enter your license key into the Premium Content interface:  

![Premium Content Activation Image](https://gitlab.com/peginc/core-rules-issues/-/raw/master/assets/instructions/activate_module.png)

# Installation
Follow these steps to get started using this module in your copy of Foundry Virtual Tabletop.
1. <b>Download the module</b>  
Now that you’ve purchased the module, you can use Foundry’s built-in Install Module interface to download it.  
From Foundry’s <b>Setup</b> page, switch to the <b>Add-on Modules</b> tab and click the <b>Install Module</b> button:    
![Install Module](https://gitlab.com/peginc/core-rules-issues/-/raw/master/assets/instructions/install_module.png)  
Within the Install Module interface, search for the *Savage Worlds* Adventure Edition Core Rules module (easily found under the “Premium Content” package category) and install it.  

2. Load a world  
This module is compatible with both your existing Foundry worlds and any new worlds you create. Make sure that the world uses the *SWADE* game system.  

3. Enable the module
    a. Switch to the <b>Settings</b> sidebar and click the Gear icon in the top right.  
    ![Settings](https://gitlab.com/peginc/core-rules-issues/-/raw/master/assets/instructions/settings.png)  
    b. Click the <b>Manage Modules</b>  
    c. Click the checkbox next to the <b>*Savage Worlds* Adventure Edition Core Rules</b>
    ![Manage Modules](https://gitlab.com/peginc/core-rules-issues/-/raw/master/assets/instructions/manage_modules.png)  
    d. When a message displays asking you to activate the "Compendium Folders" dependency, click <b>Yes</b>  
    e. Click the <b>Save Module Settings</b> button

4. Read the "Welcome Screen"  
When you have successfully enabled the module, the Welcome Screen displays. *To get the most out of this module, be sure to read through it completely.*  
![Welcome Screen](https://gitlab.com/peginc/core-rules-issues/-/raw/master/assets/instructions/welcome_screen.png)  
If you do not wish to see the Welcome Screen again, click the checkbox at the bottom to disable it.

5. Check out your shiny new official *SWADE* content  
Navigate to the <b>Compendium Packs</b> sidebar by clicking the book icon located directly to the left of the gear icon that you used to access the <b>Settings</b> sidebar in the top right.


# Content Overview
The following Compendiums are available within the module, loaded with official content to help bring your *Savage Worlds* games to life.  

*<b>Please Note: </b> Try to keep content inside compendiums when possible. Having hundreds or thousands of unneeded Actors, Items, or Journal Entries imported in your world can negatively affect Foundry performance.*

## Rules Compendium
The *Savage Worlds* rules are organized into the same chapters as the printed book. If you know the title of the article you are looking for, use the search at the top of the Compendiums sidebar so you don't have to dig through multiple folders.  
The rules have deep entity linking, which means that by clicking on the red, underlined text in each article you will be shown more detailed information about the topic you chose. Some of these are subheadings inside a longer entry, so you may need to scroll down and read through the article to find what you're looking for.

## Actor Compendiums
This compendium contains the entire bestiary section of the *Savage Worlds* Adventure Edition core rules with full stats, descriptions, special abilities, and artwork—including <b>properly sized, tokenized versions and never-before-seen artwork</b>!

## Vehicles  
You’ll also find all the vehicles presented in the *SWADE* core rules with full entity-linked descriptions as well as proper armament. The vehicles include artwork and properly sized, tokenized versions.

This module includes all the artwork found in [Gear Cards: Vehicles](https://www.peginc.com/store/gear-cards-vehicles-pdf-swade/)

## Item Compendiums
The Item compendiums contain all of the Skills, Hindrances, Edges, gear, and powers in the *SWADE* core rules, complete with fully entity-linked descriptions, ready to be dropped onto your actors.

*<b>Note: </b>Because of how Foundry is set up “under the hood”, some things are found in the Item Compendiums that are not traditionally thought of as items in *Savage Worlds* such as Powers, Edges, Hindrances, and Skills. Basically, under Foundry’s logic, if it can be dropped onto or manually added to your character sheet, it’s categorized as an item.*

### Powers
Contains all of the powers in the *SWADE* core rules, complete with fully entity linked descriptions, ready to be dropped onto your actors.
### Edges
Contains all of the Edges in the *SWADE* core rules, complete with fully entity linked descriptions, ready to be dropped onto your Actors.
### Hindrances
Contains all of the Hindrances in the *SWADE* core rules, complete with fully entity linked descriptions, ready to be dropped onto your Actors.
### Skills
Contains all of the Skills in the *SWADE* core rules, complete with fully entity linked descriptions, ready to be dropped onto your Actors.
### Equipment
Contains all of the equipment in the *SWADE* core rules, complete with fully entity linked descriptions, ready to be dropped onto your Actors.  

It also includes images from [Gear Cards: Weapons](https://www.peginc.com/store/gear-cards-weapons-pdf-swade/) and [Gear Cards: Armor](https://www.peginc.com/store/gear-cards-armor-pdf-swade/)!  

For weapons, pre-configured Chat Card Actions are included to help you bring devastation to your enemies. 

## Roll Tables
Includes fully entity-linked versions of the *Battle Effects, Creative Combat, Dynamic Backlash, Fear Table, Injury Table, Out of Control, Reaction Table, Vehicle Critical Hits* roll tables. 

## Macros
### Fear Table
This is a simple macro that prompts the user for a Fear penalty before rolling on the table.  
### Patch All Actors
PLEASE BACKUP YOUR WORLD BEFORE RUNNING THIS SCRIPT  

This script will update all the actors in your world with their owned Skills, Edges, Hindrances, Powers, and Equipment that have names that match entities in the premium content pack.

## Module Settings
### Show Welcome Screen on startup
Determines if the *Savage Worlds* Welcome Screen is shown when a user logs into the world.

### Enable SWADE Entity Linking CSS
Enables the nice red, underlined links rather than the blocky standard Foundry ones. However, for accessibility reasons, you might have players who prefer the standard Foundry style.

## Other Modules

### Required Modules
<b>Compendium Folders</b>  
This module is installed as a dependency, as it's used to organize the *Savage Worlds* compendium packs in folders until Foundry adds this feature in the core functionality.  
https://foundryvtt.com/packages/compendium-folders/

### Recommended Modules
<b>Dice So Nice!</b>
This module enables 3D dice, and more importantly, *3D Bennies*. Try it out!  
https://foundryvtt.com/packages/dice-so-nice/  

<b>PopOut!</b>
This module is not required, but your players may enjoy the ability to pop out journal entries for rules into a separate window. Another bonus is that you can use CTRL+F to search for text within the popped-out window, which is not possible in the Foundry view.  
https://foundryvtt.com/packages/popout/

## FAQ

### How can I get help regarding my purchased content?
For customer support related to purchased content - please utilize the following channels:  

* For issues with purchasing the content or other problems with the PEG web store, please email info@PEGinc.com

* For instructions on the activation or installation of purchased content, please see the following FAQ (https://foundryvtt.com/article/faq/#premium-content). If you experience any issues with this process please submit a Support Request using the Contact Us form (https://foundryvtt.com/contact-us/).

* For issues or feedback related to the content itself, please use either come to the #SWADE channel in Foundry discord (https://discord.gg/foundryvtt) for peer support or go to Gitlab (https://gitlab.com/peginc/core-rules-issues/-/issues) to create a ticket.

### When looking in a compendium, it seems none of the items are showing, or only one or two are showing.
Please check that the search bar at the top of the compendium is empty. Usually this problem is caused by leftover words in the search box at the top of the <b>Compendium</b> view. Clearing the search field will allow all entries to be displayed.

### How can I use all this cool art in my own homebrew content?
You can find all the art assets in WEBP optimized format under the modules/swade-core-rules/assets/art folder. See the [License](#License) below as it applies to those assets.

## Support
### Gitlab Issues
If you run into a problem with the module, find a bug, have an improvement request, or any other concern, the best way to make us aware of it is through a GitLab Issue. Go to https://gitlab.com/groups/peginc/-/issues and create a new ticket. *Be sure to select the correct project to which the ticket applies.*

### Peer Support
If you're looking for help in figuring out how to do something, the *Savage Worlds* community on the official Foundry discord is very active and helpful. You can find answers to *Savage Worlds* or FoundryVTT related questions under the #swade channel. https://discord.gg/foundryvtt

## License
*Savage Worlds*, all unique characters, creatures, and locations, artwork, logos, and the Pinnacle logo are © 2020 Great White Games, LLC; DBA Pinnacle Entertainment Group. Distributed by Studio 2 Publishing, Inc.

The assets provided in this module are for personal use only within the Foundry Virtual Tabletop software.

![SWADE_Logo](https://gitlab.com/peginc/core-rules-issues/-/raw/master/assets/SWADE_Logo.png)
![PEG_Logo](https://gitlab.com/peginc/core-rules-issues/-/raw/master/assets/PEG_Logo.png)